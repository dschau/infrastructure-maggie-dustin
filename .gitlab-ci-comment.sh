#!/bin/sh

apk add jq > /dev/null

echo '```' > plan-short.txt

cat $1 | sed -e '1,/^---*$/d' | sed -e '/^---*$/,$d' | sed -r 's/\x1B\[([0-9]{1,2}(;[0-9]{1,2})?)?[mGK]//g' >> plan-short.txt

echo '```' >> plan-short.txt

echo '{}' | jq -c --arg a "$(cat plan-short.txt)" '.note = $a' > plan-comment.json

API_URL=$(echo $CI_PROJECT_URL | grep -o '^https://[^/]*')
echo "API_URL: $API_URL"

curl --request POST \
    -H "PRIVATE-TOKEN: $GITLAB_TOKEN" \
    -H "content-type: application/json" \
    --data @plan-comment.json \
    $API_URL/api/v4/projects/$CI_PROJECT_ID/repository/commits/$CI_COMMIT_SHA/comments \
    > /dev/null