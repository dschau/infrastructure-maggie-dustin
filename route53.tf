data "aws_route53_zone" "primary" {
  name = "${var.domain_name}."
  comment = "The domain for maggieanddustin.com"
}

# netlify redirects
resource "aws_route53_record" "photos" {
  zone_id = "${data.aws_route53_zone.primary.zone_id}"
  name    = "photos"
  type    = "CNAME"
  ttl     = 300

  records = ["maggie-and-dustin-photos.netlify.com"]
}

# www redirects
resource "aws_route53_record" "www_photos" {
  zone_id = "${data.aws_route53_zone.primary.zone_id}"
  name    = "www.photos"
  type    = "CNAME"
  ttl     = 300

  records = ["photos.${var.domain_name}"]
}
