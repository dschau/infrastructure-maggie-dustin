provider "aws" {
  region  = "us-east-1"
  version = "1.13"
}

terraform {
  backend "s3" {
    bucket = "dschau-terraform-state"
    key    = "network/infrastructure-maggie-dustin.tfstate"
    region = "us-east-1"
  }
}
